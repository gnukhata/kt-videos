# kt-videos

This repository contains the recording of GNUKhata's code walkthrough

This repository uses git-lfs to track files. For more info refer https://docs.gitlab.com/ee/topics/git/lfs/

You need to have `git-lfs` package installed to push changes to this repository. Windows users will have git-lfs as part of git installation, Other OS users refer https://github.com/git-lfs/git-lfs/blob/main/INSTALLING.md
